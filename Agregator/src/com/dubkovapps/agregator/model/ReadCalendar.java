package com.dubkovapps.agregator.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class ReadCalendar {

	public static final String EVENT_TITLE = "title";
	public static final String EVENT_DESCRIPTION = "description";

	public static HashMap<String, String> getCurrentDayEvent(Context context) {
		HashMap<String, String> currentDayEvent = new HashMap<String, String>();

		Cursor cursor = context.getContentResolver()
				.query(Uri.parse("content://com.android.calendar/events"),
						new String[] { "calendar_id", "title", "description",
								"dtstart", "dtend", "eventLocation" }, null,
						null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			// fetching calendars name
			String CNames[] = new String[cursor.getCount()];

			String currentDay = getDate(System.currentTimeMillis());

			for (int i = 0; i < CNames.length; i++) {
				if (currentDay.compareTo(getDate(Long.parseLong(cursor
						.getString(3)))) == 0) {
					currentDayEvent.put(EVENT_TITLE, cursor.getString(1));
					currentDayEvent.put(EVENT_DESCRIPTION, cursor.getString(2));
					cursor.close();
					return currentDayEvent;
				}
				cursor.moveToNext();
			}
			cursor.close();
		}
		return null;

	}

	public static String getDate(long milliSeconds) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}
}
