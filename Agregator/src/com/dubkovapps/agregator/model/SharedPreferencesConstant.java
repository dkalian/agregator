package com.dubkovapps.agregator.model;

public class SharedPreferencesConstant {

	public static final String PREFERENCES_NAME = "AGREGATOR_SP";
	public static final String PREFERENCE_TEMPERATURE = "temperature";
	public static final String PREFERENCE_PICTURE = "picture_name";

}
