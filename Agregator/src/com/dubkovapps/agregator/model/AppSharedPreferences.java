package com.dubkovapps.agregator.model;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppSharedPreferences {

	public static String getStringPreferenceByKey(SharedPreferences sharedPreferences, String key) { 
		if (sharedPreferences.contains(key)) {
			return sharedPreferences.getString(key, null);
		}
		return null;
	}
	public static void saveStringPreference(SharedPreferences sharedPreferences ,String key, String value) { 
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}
}
