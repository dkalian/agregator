package com.dubkovapps.agregator.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dubkovapps.agregator.fragment.CalendarAndAlarmFragment;
import com.dubkovapps.agregator.fragment.RSSFragment;
import com.dubkovapps.agregator.fragment.WeatherFragment;

public class MainActivityPageAdapter extends FragmentPagerAdapter {

	public MainActivityPageAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return WeatherFragment.newInstance();
		case 1:
			return RSSFragment.newInstance();
		case 2:
			return CalendarAndAlarmFragment.newInstance();
		default:
			return null;
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "";
	}
	
	@Override
	public float getPageWidth(int position) {
	    float nbPages = 2; // You could display partial pages using a float value
	    return (1 / nbPages);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

}
