package com.dubkovapps.agregator.http;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class PrCyClient {
	 private static PrCyClient instance;
	 /**
	  * Http singleton client
	  */
	 public HttpClient client;

	 private PrCyClient() {
	  client = new DefaultHttpClient();
	 }

	 /**
	  * @return instance SapeClient
	  */
	 public static PrCyClient getInstance() {
	  if (instance == null) {
	   instance = new PrCyClient();
	  }
	  return instance;
	 }
	}