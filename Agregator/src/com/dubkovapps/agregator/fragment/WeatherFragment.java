package com.dubkovapps.agregator.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dubkovapps.agregator.R;
import com.dubkovapps.agregator.constant.Constant;
import com.dubkovapps.agregator.http.PrCyClient;
import com.dubkovapps.agregator.model.AppSharedPreferences;
import com.dubkovapps.agregator.model.GPSTracker;
import com.dubkovapps.agregator.model.SharedPreferencesConstant;

public class WeatherFragment extends Fragment {

	public static final String LOG_TAG = "WEATHER_FRAGMENT";

	public static WeatherFragment newInstance() {
		return new WeatherFragment();
	}

	Location location;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	ImageView weatherPicture;
	TextView weatherText;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.weather_fragment, null);
		weatherPicture = (ImageView) view
				.findViewById(R.id.weather_fragment_picture);
		weatherText = (TextView) view.findViewById(R.id.weather_fragment_text);
		return view;
	}

	double lat;
	double lon;

	@Override
	public void onStart() {
		super.onStart();
		GPSTracker gpsTracker = new GPSTracker(getActivity());
		gpsTracker.getLocation();
		if (gpsTracker.canGetLocation()) {
			gpsTracker.getLocation();

			lat = gpsTracker.getLatitude();
			lon = gpsTracker.getLongitude();

			LocationManager manager = (LocationManager) getActivity()
					.getSystemService(Context.LOCATION_SERVICE);

			if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
					&& (lat == 0) && (lon == 0)) {
				if (checkForCachedWeather()) {
					// If weather cached
					showCachedWeather();
				} else {
					showGpsNotEnabledError();
				}
			} else {
				new WeatherParser().execute();
			}
		} else {
			Log.d(LOG_TAG, "GPS Tracker can't get location!");
			showLocationNotFoundError();
		}
	}

	private void showCachedWeather() {
		String cachedTemperature = AppSharedPreferences
				.getStringPreferenceByKey(
						getActivity().getSharedPreferences(
								SharedPreferencesConstant.PREFERENCES_NAME,
								Context.MODE_PRIVATE),
						SharedPreferencesConstant.PREFERENCE_TEMPERATURE);
		String cachedPicureName = AppSharedPreferences
				.getStringPreferenceByKey(
						getActivity().getSharedPreferences(
								SharedPreferencesConstant.PREFERENCES_NAME,
								Context.MODE_PRIVATE),
						SharedPreferencesConstant.PREFERENCE_PICTURE);
		showWeather(cachedPicureName, Integer.parseInt(cachedTemperature));
	}

	private boolean checkForCachedWeather() {
		String cachedTemperature = AppSharedPreferences
				.getStringPreferenceByKey(
						getActivity().getSharedPreferences(
								SharedPreferencesConstant.PREFERENCES_NAME,
								Context.MODE_PRIVATE),
						SharedPreferencesConstant.PREFERENCE_TEMPERATURE);
		String cachedPicureName = AppSharedPreferences
				.getStringPreferenceByKey(
						getActivity().getSharedPreferences(
								SharedPreferencesConstant.PREFERENCES_NAME,
								Context.MODE_PRIVATE),
						SharedPreferencesConstant.PREFERENCE_PICTURE);

		Log.d(LOG_TAG, "CACHED TEMP: " + cachedTemperature);
		Log.d(LOG_TAG, "CACHED ICON: " + cachedPicureName);

		if (((cachedPicureName != null) && (cachedPicureName.length() > 0))
				&& ((cachedTemperature != null) && (cachedTemperature.length() > 0))) {
			return true;
		}
		return false;
	}

	// GPS not enabled
	private void showGpsNotEnabledError() {
		weatherText.setText(getResources().getString(R.string.gps_not_enabled));
		weatherPicture.setImageDrawable(getResources().getDrawable(
				R.drawable.ic_gps_not_enabled));
	}

	private void showConnectingNessage() {
		weatherText.setText(getResources().getString(R.string.connecting));
		weatherPicture.setImageDrawable(getResources().getDrawable(
				R.drawable.ic_connecting));
	}

	private void showLocationNotFoundError() {
		weatherText.setText(getResources().getString(
				R.string.location_not_found));
		weatherPicture.setImageDrawable(getResources().getDrawable(
				R.drawable.ic_location_not_found));
	}

	private void showWeather(String iconName, int temperature) {
		weatherPicture.setImageDrawable(getWeatherPicture(iconName));
		weatherText.setText(temperature + "°");
	}

	private Drawable getWeatherPicture(String iconName) {
		if (iconName.compareTo(Constant.ICON_NAME_CLEAR_DAY) == 0) {
			return getResources().getDrawable(R.drawable.clear_day);
		}
		if (iconName.compareTo(Constant.ICON_NAME_CLEAR_NIGHT) == 0) {
			return getResources().getDrawable(R.drawable.clear_night);
		}
		if (iconName.compareTo(Constant.ICON_NAME_CLOUDY) == 0) {
			return getResources().getDrawable(R.drawable.cloudly);
		}
		if (iconName.compareTo(Constant.ICON_NAME_FOG) == 0) {
			return getResources().getDrawable(R.drawable.foggy);
		}
		if (iconName.compareTo(Constant.ICON_NAME_HAIL) == 0) {
			return getResources().getDrawable(R.drawable.hail);
		}
		if (iconName.compareTo(Constant.ICON_NAME_PARTY_CLOUDY_DAY) == 0) {
			return getResources().getDrawable(R.drawable.partly_cloudy_day);
		}
		if (iconName.compareTo(Constant.ICON_NAME_PARTY_CLOUDY_NIGHT) == 0) {
			return getResources().getDrawable(R.drawable.partly_cloudy_night);
		}
		if (iconName.compareTo(Constant.ICON_NAME_RAIN) == 0) {
			return getResources().getDrawable(R.drawable.rain);
		}
		if (iconName.compareTo(Constant.ICON_NAME_SLEET) == 0) {
			return getResources().getDrawable(R.drawable.sleet);
		}
		if (iconName.compareTo(Constant.ICON_NAME_SNOW) == 0) {
			return getResources().getDrawable(R.drawable.snow);
		}
		if (iconName.compareTo(Constant.ICON_NAME_THUNDERSTORM) == 0) {
			return getResources().getDrawable(R.drawable.thunderstorms);
		}
		if (iconName.compareTo(Constant.ICON_NAME_TORNADO) == 0) {
			return getResources().getDrawable(R.drawable.tornado);
		}
		if (iconName.compareTo(Constant.ICON_NAME_WIND) == 0) {
			return getResources().getDrawable(R.drawable.windy);
		}
		return getResources().getDrawable(R.drawable.clear_day);
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	public class WeatherParser extends AsyncTask<Void, Void, String> {

		boolean error = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showConnectingNessage();
		}

		InputStream inputStream = null;
		private HttpClient client;
		private HttpGet httpGet;
		private BufferedReader in;

		int temperature;
		String iconName;

		@Override
		protected String doInBackground(Void... params) {
			client = PrCyClient.getInstance().client;
			String url = Constant.API_WEATHER_URL + lat + "," + lon;

			System.out.println(url);
			httpGet = new HttpGet(url);
			try {
				HttpResponse response = client.execute(httpGet);
				in = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				while ((line = in.readLine()) != null) {
					sb.append(line + "\n");
				}
				in.close();
				String result = sb.toString();
				System.out.println(result);
				JSONObject object = new JSONObject(result);
				JSONObject currently = object.getJSONObject("currently");
				String currentTenperature = currently
						.getString("apparentTemperature");
				temperature = Math
						.round(((Float.parseFloat(currentTenperature) - 32) * 5 / 9));
				iconName = currently.getString("icon");
				Log.d(LOG_TAG, "WEATHER:");
				Log.d(LOG_TAG, "Temperature: " + temperature);
				Log.d(LOG_TAG, "Icon name: " + iconName);
				AppSharedPreferences.saveStringPreference(
						getActivity().getSharedPreferences(
								SharedPreferencesConstant.PREFERENCES_NAME,
								Context.MODE_PRIVATE),
						SharedPreferencesConstant.PREFERENCE_TEMPERATURE,
						String.valueOf(temperature));
				AppSharedPreferences.saveStringPreference(
						getActivity().getSharedPreferences(
								SharedPreferencesConstant.PREFERENCES_NAME,
								Context.MODE_PRIVATE),
						SharedPreferencesConstant.PREFERENCE_PICTURE,
						String.valueOf(iconName));

			} catch (JSONException e) {
				e.printStackTrace();
				error = true;
			} catch (IOException e) {
				e.printStackTrace();
				error = true;
			} catch (IllegalStateException e) {
				e.printStackTrace();
				error = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (!error) {
				showWeather(iconName, temperature);
			} else {
				showLocationNotFoundError();
			}
		}
	}
}
