package com.dubkovapps.agregator.fragment;

import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dubkovapps.agregator.R;
import com.dubkovapps.agregator.model.ReadCalendar;

/**
 * @author Николай Дубков
 * 
 */
public class CalendarAndAlarmFragment extends Fragment {
	/**
	 * @return a new instance of CalendarAndAlarmFragment fragment
	 */
	public static Fragment newInstance() {
		return new CalendarAndAlarmFragment();
	}

	RelativeLayout alarmContainer;

	ImageView alarmPicture, calendarPicture;
	TextView alarmText, calendarText;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.calendar_and_alarm_fragment, null);

		alarmContainer = (RelativeLayout) view
				.findViewById(R.id.calendar_and_alarm_fragment_alarm_view);

		alarmPicture = (ImageView) view.findViewById(R.id.alarm_picture);
		alarmText = (TextView) view.findViewById(R.id.alarm_text);

		calendarPicture = (ImageView) view.findViewById(R.id.calendar_picture);
		calendarText = (TextView) view.findViewById(R.id.calendar_text);

		alarmContainer.setOnClickListener(onAlarmClickListener);

		return view;
	}

	@Override
	public void onStart() {
		// ALARM
		String nextAlarm = Settings.System.getString(getActivity()
				.getContentResolver(), Settings.System.NEXT_ALARM_FORMATTED);

		if (nextAlarm.length() == 0) {
			alarmPicture.setImageDrawable(getActivity().getResources()
					.getDrawable(R.drawable.ic_no_alarm));
			alarmText.setText(getActivity().getResources().getString(
					R.string.alarm_not_set));
		} else {
			alarmPicture.setImageDrawable(getActivity().getResources()
					.getDrawable(R.drawable.ic_alarm));
			alarmText.setText(nextAlarm);
		}

		// REMINDERS
		HashMap<String, String> currentDayEvent = ReadCalendar
				.getCurrentDayEvent(getActivity());
		if (currentDayEvent == null) {
			calendarPicture.setImageDrawable(getResources().getDrawable(
					R.drawable.ic_no_events));
			calendarText.setText(getResources()
					.getString(R.string.no_reminders));
		} else {
			calendarPicture.setImageDrawable(getResources().getDrawable(
					R.drawable.ic_upcoming_event));
			calendarText.setText(currentDayEvent.get(ReadCalendar.EVENT_TITLE));
		}
		super.onStart();
	};

	OnClickListener onAlarmClickListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			startActivity(new Intent(
					android.provider.AlarmClock.ACTION_SET_ALARM));
		}
	};
}
