package com.dubkovapps.agregator.fragment;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dubkovapps.agregator.R;
import com.dubkovapps.agregator.constant.Constant;
import com.dubkovapps.agregator.http.Connection;

public class RSSFragment extends Fragment {

	private static final String LOG_TAG = "RSS FRAGMENT";

	public static RSSFragment newInstance() {
		return new RSSFragment();
	}

	RelativeLayout error_message;
	TextView title, content;
	ProgressBar progressBar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.rss_fragment, null);

		title = (TextView) view.findViewById(R.id.rss_fragment_title);
		content = (TextView) view.findViewById(R.id.rss_fragment_content);
		progressBar = (ProgressBar) view.findViewById(R.id.rss_progressbar);
		
		error_message = (RelativeLayout) view
				.findViewById(R.id.rss_fragment_no_connection_message);

		

		return view;
	}
	
	@Override
	public void onStart() {
		if (Connection.isNetworkAvailable(getActivity())) {
			error_message.setVisibility(View.GONE);
			title.setVisibility(View.VISIBLE);
			content.setVisibility(View.VISIBLE);
			new RSSParseTask().execute();
		} else {
			error_message.setVisibility(View.VISIBLE);
			title.setVisibility(View.GONE);
			content.setVisibility(View.GONE);
		}
		super.onStart();
	}

	public class RSSParseTask extends AsyncTask<Void, Void, Void> {
		boolean error = false;

		String newTitle;
		String newDescription;

		@Override
		protected void onPreExecute() {
			progressBar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {

			Document doc;
			try {
				doc = Jsoup.connect(Constant.RSS_URL).ignoreContentType(true)
						.get();
				Elements items = doc.getElementsByTag(Constant.RSS_TAG_ITEM);
				Element firstNew = items.get(0);
				newTitle = firstNew.select(Constant.RSS_TAG_TITLE).text();
				newDescription = firstNew.select(Constant.RSS_TAG_DESCRITION)
						.text();
			} catch (IOException e) {
				e.printStackTrace();
				error = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (error) {
				Toast.makeText(getActivity(), "Can't connect to RSS",
						Toast.LENGTH_LONG).show();
				progressBar.setVisibility(View.GONE);
			} else {
				progressBar.setVisibility(View.GONE);
				title.setText(newTitle);
				content.setText(Html.fromHtml(newDescription));
			}
		}
	};
}
