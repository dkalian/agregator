package com.dubkovapps.agregator;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.dubkovapps.agregator.adapter.MainActivityPageAdapter;
import com.dubkovapps.agregator.customViews.CustomDigitalClock;

/**
 * @author Николай Дубков
 * 
 */

public class MainActivity extends ActionBarActivity {

	TextView date;
	CustomDigitalClock time;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Hide Action bar
		ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		initTimeAndDate();

		initViewPager();
	}

	void initViewPager() {
		final ViewPager pager = (ViewPager) findViewById(R.id.pager);
		final MainActivityPageAdapter adapter = new com.dubkovapps.agregator.adapter.MainActivityPageAdapter(
				getSupportFragmentManager());
		pager.setAdapter(adapter);
		pager.setCurrentItem(0);
	}

	// **************** TIME AND DATE **************

	private void initTimeAndDate() {
		time = (CustomDigitalClock) findViewById(R.id.activity_main_time);

		date = (TextView) findViewById(R.id.activity_main_date);

		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		String dayOfTheWeek = sdf.format(d);
		date.setText(dayOfTheWeek.toUpperCase()
				+ ", "
				+ DateUtils.formatDateTime(this, System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_DATE).toUpperCase());

		time.setOnClickListener(timeAndDateOnClickListener);
		date.setOnClickListener(timeAndDateOnClickListener);
	}

	OnClickListener timeAndDateOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			startActivity(new Intent(
					android.provider.Settings.ACTION_DATE_SETTINGS));
		}
	};

}
