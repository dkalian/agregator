package com.dubkovapps.agregator.constant;

public class Constant {
	public static final String WEATHER_API_REY = "95114a4bfa1f6f7e31589959137ed3bb";

	public static final String API_WEATHER_URL = "https://api.forecast.io/forecast/"
			+ WEATHER_API_REY + "/";

	public static final String ICON_NAME_CLEAR_DAY = "clear-day";
	public static final String ICON_NAME_CLEAR_NIGHT = "clear-night,";
	public static final String ICON_NAME_CLOUDY = "cloudy";
	public static final String ICON_NAME_FOG = "fog";
	public static final String ICON_NAME_HAIL = "hail";
	public static final String ICON_NAME_PARTY_CLOUDY_DAY = "partly-cloudy-day";
	public static final String ICON_NAME_PARTY_CLOUDY_NIGHT = "partly-cloudy-night";
	public static final String ICON_NAME_RAIN = "rain";
	public static final String ICON_NAME_SLEET = "sleet";
	public static final String ICON_NAME_SNOW = "snow";
	public static final String ICON_NAME_THUNDERSTORM = "thunderstorm";
	public static final String ICON_NAME_TORNADO = "tornado";
	public static final String ICON_NAME_WIND = "wind";

	// RSS CONSTANT
	public static final String RSS_URL = "http://news.yandex.ru/index.rss";
	public static final String RSS_TAG_ITEM = "item";
	public static final String RSS_TAG_TITLE = "title";
	public static final String RSS_TAG_DESCRITION = "description";

}
